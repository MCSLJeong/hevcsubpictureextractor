/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ITU/ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

 /** \file     TAppMctsExtTop.cpp
  \brief    MCTS Extractor application class
  */

#include <list>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <assert.h>
#include <fstream>

#include "TAppSubPicExtTop.h"
#include "TLibDecoder/AnnexBread.h"
#include "TLibDecoder/NALread.h"
#include "TLibEncoder/NALwrite.h"
#include "TLibCommon/SEI.h"

#if MCTS_EXTRACTION
  //! \ingroup TAppMctsExt
  //! \{

  // ====================================================================================================================
  // Constructor / destructor / initialization / destroy
  // ====================================================================================================================

TAppSubPicExtTop::TAppSubPicExtTop()
{
}


Void TAppSubPicExtTop::create()
{
	m_allMctsExtractionInfoPresent = false;
}

Void TAppSubPicExtTop::destroy()
{
	m_inputBitstreamFileName.clear();
	m_outputBitstreamFileName.clear();
}

// ====================================================================================================================
// Public member functions
// ====================================================================================================================

/**
 - create internal class
 - initialize internal class
 - until the end of the bitstream, call extraction function in TDecMctsExt class
 - delete allocated buffers
 - destroy internal class
 .
 */
Void TAppSubPicExtTop::extract()
{
	for (UInt i = 0; i < m_targetMctsIdx.size(); i++)
	{
		m_mctsExtractionInfoPresent.push_back(false);
	}

	ifstream bitstreamFile(m_inputBitstreamFileName.c_str(), ifstream::in | ifstream::binary);
	if (!bitstreamFile)
	{
		fprintf(stderr, "\nfailed to open input bitstream file `%s' for reading\n", m_inputBitstreamFileName.c_str());
		exit(EXIT_FAILURE);
	}

	InputByteStream bytestream(bitstreamFile);

	TComSlice* emptySlice = new TComSlice();
	Bool	   isPSPresent = false;
	Int		   tileBitstreamSize = 0;
	Int		   curPOC = -1;

	// create & initialize internal classes
	xCreateMctsExtLib();
	xInitMctsExtLib();

	Int iSkipFrame = 0;
	Int iPOCLastDisplay = 0;

	fstream bitstreamFileOut(m_outputBitstreamFileName.c_str(), fstream::binary | fstream::out);
	if (!bitstreamFileOut)
	{
		fprintf(stderr, "\nfailed to open output bitstream file `%s' for writing\n", m_outputBitstreamFileName.c_str());
		exit(EXIT_FAILURE);
	}

	AccessUnit outAccessUnit;
	while (!!bitstreamFile)
	{
		streampos location = bitstreamFile.tellg();
		AnnexBStats stats = AnnexBStats();
		InputNALUnit inNalu;

		//UInt numCtusInOutputPictures = 512;
		//UInt numCtusInInputPictures = 2048;
		//UInt numCtusInOutputPictures = 7200;
		//UInt numCtusInInputPictures = 28800;
		UInt numCtusInOutputPictures = 0;
		UInt numCtusInInputPictures = 0;

		byteStreamNALUnit(bytestream, inNalu.getBitstream().getFifo(), stats);

		Bool bNewPicture = false;
		if (inNalu.getBitstream().getFifo().empty())
		{
			fprintf(stderr, "Warning: Attempt to extract an empty NAL unit\n");
		}
		else
		{
			read(inNalu);
			m_pcSlice = m_cTDecTop.getApcSlicePilot();
			// decode HLS, skipping cabac decoding and reconstruction
			bNewPicture = m_cTDecTop.decode(inNalu, iSkipFrame, iPOCLastDisplay, true);

			if (bNewPicture)
			{
				bitstreamFile.clear();
				bitstreamFile.seekg(location - streamoff(3));
				bytestream.reset();
			}
		}

		if ((bNewPicture || !bitstreamFile || inNalu.m_nalUnitType == NAL_UNIT_EOS) &&
			!m_cTDecTop.getFirstSliceInSequence())
		{
			m_cTDecTop.getPcPic()->setReconMark(true);
			if (bitstreamFile || inNalu.m_nalUnitType == NAL_UNIT_EOS)
			{
				m_cTDecTop.setFirstSliceInPicture(true);
			}
		}

		if ((inNalu.m_nalUnitType == NAL_UNIT_VPS || inNalu.m_nalUnitType == NAL_UNIT_SPS ||
			inNalu.m_nalUnitType == NAL_UNIT_PPS))
		{
			continue;
		}
		
		else if (!m_allMctsExtractionInfoPresent && inNalu.isSei())
		{
			// search matching EIS and push parameter sets into AU
			m_cTDecTop.xParsePrefixSEImessages();
			if (!m_cTDecTop.getSEIs().empty())
			{
				xExtractSuitableParameterSets(
					getSeisByType(m_cTDecTop.getSEIs(), SEI::TEMP_MOTION_CONSTRAINED_TILE_SETS),
					getSeisByType(m_cTDecTop.getSEIs(), SEI::MCTS_EXTRACTION_INFO_SET),
					outAccessUnit);
				isPSPresent = true;
				//xWriteOutput(bitstreamFileOut, outAccessUnit);
				//outAccessUnit.clear();
			}
		}
		
		else if (!bNewPicture && m_allMctsExtractionInfoPresent && inNalu.isSlice())
		{
			// Calculating sliceCurStartCtuTsAddrOffset.
			m_sourceWidth = m_pcSlice->getSPS()->getPicWidthInLumaSamples();
			m_sourceHeight = m_pcSlice->getSPS()->getPicHeightInLumaSamples();
			m_uiMaxCUWidth = m_pcSlice->getSPS()->getMaxCUWidth();
			m_uiMaxCUHeight = m_pcSlice->getSPS()->getMaxCUHeight();

			m_numCtuInSrcRow = (m_sourceHeight) / (m_uiMaxCUHeight);
			m_numCtuInSrcCol = (m_sourceWidth) / (m_uiMaxCUWidth);

			m_numCtuInCodingRow = (m_iCodingFaceHeight) / (m_uiMaxCUHeight);
			m_numCtuInCodingCol = (m_iCodingFaceWidth) / (m_uiMaxCUWidth);

			numCtusInOutputPictures = m_numCtuInCodingRow * m_numCtuInCodingCol;
			numCtusInInputPictures = m_cTDecTop.getPcPic()->getNumberOfCtusInFrame();

			m_sliceCurStartCtuTsAddrOffsetInRow = m_iCodingFaceLeftYInLumaSamples / m_uiTsHeight;
			m_sliceCurStartCtuTsAddrOffsetInCol = m_iCodingFaceLeftXInLumaSamples / m_uiTsWidth;

			//m_pcSlice->setPic(m_cTDecTop.getPcPic());
			for (UInt idx = 0; idx < m_targetMctsIdx.size(); idx++)
			{
				if (xIsNaluWithinMCTSSet(m_targetMctsIdx[idx]))
				{
					//rewrite VPS, SPS, PPS
					if (isPSPresent)
					{
						//m_sliceCurStartCtuTsAddrOffset = (m_iCodingFaceLeftXInLumaSamples / m_uiMaxCUWidth) * (m_numCtuInSrcCol * (m_iCodingFaceLeftYInLumaSamples / m_uiMaxCUHeight));
						//m_sliceCurStartCtuTsAddrOffsetInRow = m_sliceCurStartCtuTsAddrOffset / m_numCtuInCodingCol;
						//m_sliceCurStartCtuTsAddrOffsetInCol = m_sliceCurStartCtuTsAddrOffset % m_numCtuInCodingCol;
						//printf("m_sliceCurStartCtuTsAddrOffset: %d\n", m_sliceCurStartCtuTsAddrOffset);
						//printf("m_sliceCurStartCtuTsAddrOffsetInRow: %d, m_sliceCurStartCtuTsAddrOffsetInCol: %d\n", m_sliceCurStartCtuTsAddrOffsetInRow, m_sliceCurStartCtuTsAddrOffsetInCol);

						//m_sliceCurEndCtuTsAddrOffsetInRow = m_sliceCurStartCtuTsAddrOffsetInRow + m_numCtuInCodingRow;
						//m_sliceCurEndCtuTsAddrOffsetInCol = m_sliceCurStartCtuTsAddrOffsetInCol + m_numCtuInCodingCol;
						//m_sliceCurEndCtuTsAddrOffset = m_sliceCurEndCtuTsAddrOffsetInCol + m_numCtuInSrcCol * (m_sliceCurEndCtuTsAddrOffsetInRow - 1);
						//printf("m_sliceCurEndCtuTsAddrOffset: %d\n", m_sliceCurEndCtuTsAddrOffset);
						//printf("m_sliceCurEndCtuTsAddrOffsetInRow: %d, m_sliceCurEndCtuTsAddrOffsetInCol: %d\n", m_sliceCurEndCtuTsAddrOffsetInRow, m_sliceCurEndCtuTsAddrOffsetInCol);

						// Storing VPS, SPS, PPS. 
						OutputNALUnit outputNALVPS(NAL_UNIT_VPS, 0, 0);
						m_cEntropyCoder.setEntropyCoder(&m_cCavlcCoder);
						m_cEntropyCoder.setBitstream(&(outputNALVPS.m_Bitstream));
						m_cEntropyCoder.encodeVPS(outputVPS);
						outAccessUnit.push_back(new NALUnitEBSP(outputNALVPS));

						OutputNALUnit outputNALSPS(NAL_UNIT_SPS, 0, 0);
						m_cEntropyCoder.setEntropyCoder(&m_cCavlcCoder);
						m_cEntropyCoder.setBitstream(&(outputNALSPS.m_Bitstream));
						outputSPS->setPicWidthInLumaSamples(m_iCodingFaceWidth);
						outputSPS->setPicHeightInLumaSamples(m_iCodingFaceHeight);
						m_cEntropyCoder.encodeSPS(outputSPS);
						outAccessUnit.push_back(new NALUnitEBSP(outputNALSPS));

						//printf("m_pcSlice->getPPS()->getLoopFilterAcrossSlicesEnabledFlag(): %d\n", m_pcSlice->getPPS()->getLoopFilterAcrossSlicesEnabledFlag());
						//printf("m_pcSlice->getPic()->getPicSym()->getPPS().getNumTileColumnsMinus1(): %d\n", m_pcSlice->getPic()->getPicSym()->getPPS().getNumTileColumnsMinus1());
						
						int numTileRowsMinus1 = m_pcSlice->getPPS()->getNumTileRowsMinus1();
						int numTileColumnsMinus1 = m_pcSlice->getPPS()->getNumTileColumnsMinus1();

						int startCtuAddrInRows = (m_iCodingFaceHeight / m_uiMaxCUHeight);
						int startCtuAddrInColumns = (m_iCodingFaceWidth / m_uiMaxCUWidth);

						int numTileRowsOffset = startCtuAddrInRows / (m_numCtuInSrcRow / numTileRowsMinus1 + 1);
						int numTileColumnsOffset = startCtuAddrInColumns / (m_numCtuInSrcCol / numTileColumnsMinus1 + 1);

						// This must be activated when tiles are ununiformly spaced
						if (!m_pcSlice->getPPS()->getTileUniformSpacingFlag())
						{
							// This code must be checked
							int tileWidth = m_sourceWidth / (numTileColumnsMinus1 + 1);
							int tileHeight = m_sourceHeight / (numTileRowsMinus1 + 1);

							outputPPS->setNumTileRowsMinus1((m_iCodingFaceHeight / tileHeight) - 1);
							outputPPS->setNumTileColumnsMinus1((m_iCodingFaceWidth / tileWidth) - 1);

							int tileColumnWidthSum = 0;

							for (UInt col = 0; col < m_pcSlice->getPPS()->getNumTileColumnsMinus1(); col++)
							{
								tileColumnWidthSum += m_pcSlice->getPPS()->getTileColumnWidth(col);

								//if ((tileColumnWidthSum >= startCtuAddrInColumns) && (tileColumnWidthSum <= end))
								{
									m_tileColumnWidth.push_back(m_pcSlice->getPPS()->getTileColumnWidth(col));
								}
							}
							outputPPS->setTileColumnWidth(m_tileColumnWidth);

							int tileRowHeightSum = 0;

							for (UInt row = 0; row < m_pcSlice->getPPS()->getNumTileRowsMinus1(); row++)
							{
								tileRowHeightSum += m_pcSlice->getPPS()->getTileRowHeight(row);

								if (tileRowHeightSum >= startCtuAddrInRows)
								{
									m_tileRowHeight.push_back(m_pcSlice->getPPS()->getTileRowHeight(row));
								}
							}
							outputPPS->setTileRowHeight(m_tileRowHeight);
						}
						// When the tiles are uniformly spaced
						else
						{
							int tileWidth = m_sourceWidth / (numTileColumnsMinus1 + 1);
							int tileHeight = m_sourceHeight / (numTileRowsMinus1 + 1);
							m_tileIdxOffsetInRow = (m_iCodingFaceLeftYInLumaSamples) / tileHeight;
							m_tileIdxOffsetInCol = (m_iCodingFaceLeftXInLumaSamples) / tileWidth;

							m_tileIdxOffset = m_tileIdxOffsetInCol + m_tileIdxOffsetInRow * (numTileColumnsMinus1 + 1);
							
							outputPPS->setNumTileRowsMinus1((m_iCodingFaceHeight / tileHeight) - 1);
							outputPPS->setNumTileColumnsMinus1((m_iCodingFaceWidth / tileWidth) - 1);
						}
						
						OutputNALUnit outputNALPPS(NAL_UNIT_PPS, 0, 0);
						m_cEntropyCoder.setEntropyCoder(&m_cCavlcCoder);
						m_cEntropyCoder.setBitstream(&(outputNALPPS.m_Bitstream));
						outputPPS->setLoopFilterAcrossSlicesEnabledFlag(false);
						outputPPS->setLoopFilterAcrossTilesEnabledFlag(false);
						
						
						//outputPPS->setLoopFilterAcrossSlicesEnabledFlag(m_pcSlice->getPPS()->getLoopFilterAcrossSlicesEnabledFlag());
						//outputPPS->setLoopFilterAcrossTilesEnabledFlag(m_pcSlice->getPPS()->getLoopFilterAcrossTilesEnabledFlag());
						outputPPS->setTilesEnabledFlag(true);
						outputPPS->setTileUniformSpacingFlag(m_pcSlice->getPPS()->getTileUniformSpacingFlag());

						// To activate the loop filter
						m_cPic.create(*outputSPS, *outputPPS, true, false);
						outputPPS->setTilesEnabledFlag(false);
						outputPPS->setLoopFilterAcrossSlicesEnabledFlag(true);
						
						m_cEntropyCoder.encodePPS(outputPPS);
						outAccessUnit.push_back(new NALUnitEBSP(outputNALPPS));

						isPSPresent = false;
					}
					// setup
					Int targetMctsIdxInRow = m_targetMctsIdx[idx] / (m_pcSlice->getPPS()->getNumTileColumnsMinus1() + 1);
					Int targetMctsIdxInCol = m_targetMctsIdx[idx] % (m_pcSlice->getPPS()->getNumTileColumnsMinus1() + 1);
					
					m_cSlicePicSym.setNumberOfCtusInFrame(m_cTDecTop.getPcPic()->getPicSym(), numCtusInOutputPictures);
					m_pcSlice->setPic(m_cTDecTop.getPcPic());
					
					// To activate the loop filter
					//m_cPic.create(*outputSPS, *outputPPS, true, false);
					m_pcSlice->setPic(&m_cPic);
					
					// Modifying slice addresses. 
					//Int sliceCurStartCtuTsAddr = m_pcSlice->getSliceCurStartCtuTsAddr();
					//Int sliceCurEndCtuTsAddr = m_pcSlice->getSliceCurEndCtuTsAddr();
					//printf("sliceCurStartCtuTsAddr: %d, sliceCurEndCtuTsAddr: %d\n", sliceCurStartCtuTsAddr, sliceCurEndCtuTsAddr);
					Int outputMctsIdx = (targetMctsIdxInCol - m_tileIdxOffsetInCol) + (targetMctsIdxInRow - m_tileIdxOffsetInRow) * (outputPPS->getNumTileColumnsMinus1() + 1);
					
					Int sliceCurFirstCtuRsAddr = m_cPic.getPicSym()->getTComTile(outputMctsIdx)->getFirstCtuRsAddr();
					Int sliceCurFirstCtuTsAddr = m_cPic.getPicSym()->getCtuRsToTsAddrMap(sliceCurFirstCtuRsAddr);
					m_pcSlice->setSliceCurStartCtuTsAddr(sliceCurFirstCtuTsAddr);
					m_pcSlice->setSliceSegmentCurStartCtuTsAddr(sliceCurFirstCtuTsAddr);

					//Int sliceSegmentCurStartCtuTsAddr = m_pcSlice->getSliceSegmentCurStartCtuTsAddr();
					//Int sliceSegmentCurEndCtuTsAddr = m_pcSlice->getSliceSegmentCurEndCtuTsAddr();
					//Int sliceSegmentRsAddr = m_pcSlice->getPic()->getPicSym()->getCtuTsToRsAddrMap(sliceSegmentCurStartCtuTsAddr);
					//printf("sliceSegmentCurStartCtuTsAddr: %d, sliceSegmentCurEndCtuTsAddr: %d, sliceSegmentRsAddr: %d\n", sliceSegmentCurStartCtuTsAddr, sliceSegmentCurEndCtuTsAddr, sliceSegmentRsAddr);

					m_pcSlice->setSliceIdx(idx);
					printf("m_pcSlice->getDeblockingFilterDisable():%d, m_pcSlice->getDependentSliceSegmentFlag(): %d\n", m_pcSlice->getDeblockingFilterDisable(), m_pcSlice->getDependentSliceSegmentFlag());

					//Int sliceCurStartCtuTsAddrInRow = sliceCurStartCtuTsAddr / m_numCtuInSrcCol;
					//Int sliceCurStartCtuTsAddrInCol = sliceCurStartCtuTsAddr % m_numCtuInSrcCol;

					//Int sliceSegmentRsAddrInRow = sliceSegmentRsAddr / m_numCtuInSrcCol;
					//Int sliceSegmentRsAddrInCol = sliceSegmentRsAddr % m_numCtuInSrcCol;

					// New slice addresses in output sub-picture. 
					//Int sliceCodingStartCtuTsAddr = (sliceCurStartCtuTsAddrInCol - m_sliceCurStartCtuTsAddrOffsetInCol) + m_numCtuInCodingCol * (sliceCurStartCtuTsAddrInRow - m_sliceCurStartCtuTsAddrOffsetInRow);
					//Int sliceCodingSegmentRsAddr = (sliceSegmentRsAddrInCol - m_sliceCurStartCtuTsAddrOffsetInCol) + m_numCtuInCodingCol * (sliceSegmentRsAddrInRow - m_sliceCurEndCtuTsAddrOffsetInRow);
					//printf("m_targetMctsIdx[idx]: %d, sliceCodingStartCtuTsAddr: %d, sliceCodingSegmentRsAddr: %d\n", m_targetMctsIdx[idx], sliceCodingStartCtuTsAddr, sliceCodingSegmentRsAddr);
					
					/*
					if (m_targetMctsIdx[idx] == 8)
					{
						m_pcSlice->setSliceCurStartCtuTsAddr(256);
						m_pcSlice->setSliceSegmentCurStartCtuTsAddr(256);
					}

					if (m_targetMctsIdx[idx] == 9)
					{
						m_pcSlice->setSliceCurStartCtuTsAddr(320);
						m_pcSlice->setSliceSegmentCurStartCtuTsAddr(320);
					}

					if (m_targetMctsIdx[idx] == 10)
					{
						m_pcSlice->setSliceCurStartCtuTsAddr(384);
						m_pcSlice->setSliceSegmentCurStartCtuTsAddr(384);
					}

					if (m_targetMctsIdx[idx] == 11)
					{
						m_pcSlice->setSliceCurStartCtuTsAddr(448);
						m_pcSlice->setSliceSegmentCurStartCtuTsAddr(448);
					}
					*/
					//m_pcSlice->setSliceCurStartCtuTsAddr(sliceCodingStartCtuTsAddr);
					//m_pcSlice->setSliceSegmentCurStartCtuTsAddr(sliceCodingStartCtuTsAddr);
					//m_pcSlice->setSliceCurEndCtuTsAddr(m_numCtuInCodingRow * m_numCtuInCodingCol);
					//m_pcSlice->setSliceSegmentCurEndCtuTsAddr(m_numCtuInCodingRow * m_numCtuInCodingCol);
					OutputNALUnit outNalu(m_pcSlice->getNalUnitType(), m_pcSlice->getTLayer());
					m_cEntropyCoder.setEntropyCoder(&m_cCavlcCoder);
					m_cEntropyCoder.setBitstream(&outNalu.m_Bitstream);
					m_cEntropyCoder.encodeSliceHeader(m_pcSlice);

					//convert
					xInputToOutputSliceNaluConversion(inNalu, outNalu, m_pcSlice);

					//write to file
					outAccessUnit.push_back(new NALUnitEBSP(outNalu));
					xWriteOutput(bitstreamFileOut, outAccessUnit);
					outAccessUnit.clear();

					m_cSlicePicSym.setNumberOfCtusInFrame(m_cTDecTop.getPcPic()->getPicSym(), numCtusInInputPictures);

					// console output
					if (curPOC != m_pcSlice->getPOC())
					{
						curPOC = m_pcSlice->getPOC();
						tileBitstreamSize = 0;

						tileBitstreamSize += (int)inNalu.getBitstream().getFifo().size();
					}
					else
					{
						tileBitstreamSize += (int)inNalu.getBitstream().getFifo().size();

						if (idx == m_targetMctsIdx.size() - 1)
						{
							TChar c = (m_pcSlice->isIntra() ? 'I' : m_pcSlice->isInterP() ? 'P' : 'B');
							if (!m_pcSlice->isReferenced())
							{
								c += 32;
							}
							printf("POC %4d TId: %1d ( %c-SLICE, QP%3d ) ", m_pcSlice->getPOC(),
								m_pcSlice->getTLayer(),
								c,
								m_pcSlice->getSliceQp());
							printf(" %10d bits\n", tileBitstreamSize);
						}
					}
					break;
				}
			}
		}
	}

	if (!m_allMctsExtractionInfoPresent)
	{
		fprintf(stderr, "\nInput bitstream file `%s' does not contain MCTS extraction information for target MCTS indices ", m_inputBitstreamFileName.c_str());
	}

	// delete buffers
	m_cTDecTop.deletePicBuffer();

	// destroy internal classes
	xDestroyMctsExtLib();

}

// ====================================================================================================================
// Protected member functions
// ====================================================================================================================


Void TAppSubPicExtTop::xCreateMctsExtLib()
{
	// create decoder class
	m_cTDecTop.create();
}

Void TAppSubPicExtTop::xDestroyMctsExtLib()
{
	// destroy decoder class
	m_cTDecTop.destroy();
}

Void TAppSubPicExtTop::xInitMctsExtLib()
{
	// initialize decoder class
	m_cTDecTop.init();
}


Void TAppSubPicExtTop::xExtractSuitableParameterSets(SEIMessages SEIMctsSEIs, SEIMessages SEIMctsEisSEIs, AccessUnit &accessUnit)
{
	if (SEIMctsSEIs.size() && SEIMctsEisSEIs.size())
	{
		SEIMCTSExtractionInfoSet* SEIMCTSExtractionInfoSetSEI = (SEIMCTSExtractionInfoSet*) *(SEIMctsEisSEIs.begin());
		for (std::vector<SEIMCTSExtractionInfoSet::MCTSExtractionInfo>::iterator EisIter = SEIMCTSExtractionInfoSetSEI->m_MCTSExtractionInfoSets.begin(); EisIter != SEIMCTSExtractionInfoSetSEI->m_MCTSExtractionInfoSets.end(); EisIter++)
		{
			for (UInt idx = 0; idx < m_targetMctsIdx.size(); idx++)
			{
				for (int j = 0; j < EisIter->m_idxOfMctsInSet.size() && !m_mctsExtractionInfoPresent[idx]; j++)
				{
					for (int k = 0; k < EisIter->m_idxOfMctsInSet[j].size() && !m_mctsExtractionInfoPresent[idx]; k++)
					{

						if (EisIter->m_idxOfMctsInSet[j][k] == m_targetMctsIdx[idx])
						{
							std::vector<TComInputBitstream> vps_rbsps;
							vps_rbsps.resize(EisIter->m_vpsRbspData.size());
							for (int jj = 0; jj < EisIter->m_vpsRbspData.size(); jj++)
							{
								for (int kk = 0; kk < EisIter->m_vpsRbspDataLength[jj]; kk++)
								{
									vps_rbsps[jj].getFifo().push_back(EisIter->m_vpsRbspData[jj][kk]);
								}
								OutputNALUnit vpsout(NAL_UNIT_VPS, 0, 0);
								vpsout.m_Bitstream.getFIFO() = vps_rbsps[jj].getFifo();
								//accessUnit.push_back(new NALUnitEBSP(vpsout));
							}

							std::vector<TComInputBitstream> sps_rbsps;
							sps_rbsps.resize(EisIter->m_spsRbspData.size());
							for (int jj = 0; jj < EisIter->m_spsRbspData.size(); jj++)
							{
								for (int kk = 0; kk < EisIter->m_spsRbspDataLength[jj]; kk++)
								{
									sps_rbsps[jj].getFifo().push_back(EisIter->m_spsRbspData[jj][kk]);
								}
								OutputNALUnit spsout(NAL_UNIT_SPS, 0, 0);
								spsout.m_Bitstream.getFIFO() = sps_rbsps[jj].getFifo();
								//accessUnit.push_back(new NALUnitEBSP(spsout));
							}

							std::vector<TComInputBitstream> pps_rbsps;
							pps_rbsps.resize(EisIter->m_ppsRbspData.size());
							for (int jj = 0; jj < EisIter->m_ppsRbspData.size(); jj++)
							{
								for (int kk = 0; kk < EisIter->m_ppsRbspDataLength[jj]; kk++)
								{
									pps_rbsps[jj].getFifo().push_back(EisIter->m_ppsRbspData[jj][kk]);
								}
								OutputNALUnit ppsout(NAL_UNIT_PPS, 0, 0);
								ppsout.m_Bitstream.getFIFO() = pps_rbsps[jj].getFifo();
								//accessUnit.push_back(new NALUnitEBSP(ppsout));
							}
							
							TComVPS* nestedVps = new TComVPS();
							TComSPS* nestedSps = new TComSPS();
							TComPPS* nestedPps = new TComPPS();

							//if (idx == 0)
							{
								// VPS
								m_cEntropyDecoder.setEntropyDecoder(&m_cCavlcDecoder);
								m_cEntropyDecoder.setBitstream(&vps_rbsps[0]);
								m_cEntropyDecoder.decodeVPS(nestedVps);
								outputVPS = nestedVps;
								/*
								OutputNALUnit outputVPS(NAL_UNIT_VPS, 0, 0);
								m_cEntropyCoder.setEntropyCoder(&m_cCavlcCoder);
								m_cEntropyCoder.setBitstream(&(outputVPS.m_Bitstream));
								m_cEntropyCoder.encodeVPS(nestedVps);
								accessUnit.push_back(new NALUnitEBSP(outputVPS));
								*/

								// SPS
								m_cEntropyDecoder.setBitstream(&sps_rbsps[0]);
								m_cEntropyDecoder.decodeSPS(nestedSps);
								outputSPS = nestedSps;
								/*
								OutputNALUnit outputSPS(NAL_UNIT_SPS, 0, 0);
								m_cEntropyCoder.setBitstream(&(outputSPS.m_Bitstream));
								m_cEntropyCoder.encodeSPS(nestedSps);
								accessUnit.push_back(new NALUnitEBSP(outputSPS));
								*/

								// PPS
								m_cEntropyDecoder.setBitstream(&pps_rbsps[0]);
								m_cEntropyDecoder.decodePPS(nestedPps);
								outputPPS = nestedPps;
								/*
								OutputNALUnit outputPPS(NAL_UNIT_PPS, 0, 0);
								m_cEntropyCoder.setBitstream(&(outputPPS.m_Bitstream));
								m_cEntropyCoder.encodePPS(nestedPps);
								accessUnit.push_back(new NALUnitEBSP(outputPPS));
								*/
							}
							printf("MCTS extraction info for target MCTS index %d found\n", m_targetMctsIdx[idx]);
							//printf("Output bitstream resolution: %dx%d\n", nestedSps->getPicWidthInLumaSamples(), nestedSps->getPicHeightInLumaSamples());
							printf("Output bitstream valid region: (%d,%d)\n", m_iCodingFaceLeftXInLumaSamples, m_iCodingFaceLeftYInLumaSamples);
							printf("Output bitstream resolution: %dx%d\n", m_iCodingFaceWidth, m_iCodingFaceHeight);
							m_mctsExtractionInfoPresent[idx] = true;
						}
					}

				}
			
			}
		}
		
		Bool allMctsExtractionInfoPresent = true;
		for (UInt idx = 0; idx < m_targetMctsIdx.size(); idx++)
		{
			if (!m_mctsExtractionInfoPresent[idx])
			{
				allMctsExtractionInfoPresent = false;
			}
		}
		if (allMctsExtractionInfoPresent)
		{
			m_allMctsExtractionInfoPresent = true;
		}
	}
}


Bool TAppSubPicExtTop::xIsNaluWithinMCTSSet(Int mcts_id)
{
	UInt currNaluTSSliceSegAddr = m_pcSlice->getSliceSegmentCurStartCtuTsAddr();
	UInt firstCtuAddrTSInMcts = m_cTDecTop.getPcPic()->getPicSym()->getCtuRsToTsAddrMap(m_cTDecTop.getPcPic()->getPicSym()->getTComTile(mcts_id)->getFirstCtuRsAddr());
	UInt lastCtuAddrTSInMcts = -1;

	if (mcts_id < (m_cTDecTop.getPcPic()->getPicSym()->getNumTiles() - 1))
	{// not last tile
		lastCtuAddrTSInMcts = m_cTDecTop.getPcPic()->getPicSym()->getCtuRsToTsAddrMap(m_cTDecTop.getPcPic()->getPicSym()->getTComTile(mcts_id + 1)->getFirstCtuRsAddr()) - 1;
	}
	else
	{
		lastCtuAddrTSInMcts = m_cTDecTop.getPcPic()->getNumberOfCtusInFrame() - 1;
	}

	return (currNaluTSSliceSegAddr >= firstCtuAddrTSInMcts && currNaluTSSliceSegAddr <= lastCtuAddrTSInMcts);
}


Void TAppSubPicExtTop::xInputToOutputSliceNaluConversion(InputNALUnit &inNalu, OutputNALUnit &outNalu, TComSlice* curSlice)
{
	const UInt uiNumSubstreams = curSlice->getNumberOfSubstreamSizes() + 1;

	TComInputBitstream **ppcSubstreams = NULL;
	ppcSubstreams = new TComInputBitstream*[uiNumSubstreams];
	for (UInt ui = 0; ui < uiNumSubstreams; ui++)
	{
		ppcSubstreams[ui] = inNalu.getBitstream().extractSubstream(ui + 1 < uiNumSubstreams ? (curSlice->getSubstreamSize(ui) << 3) : inNalu.getBitstream().getNumBitsLeft());
	}

	TComOutputBitstream  *pcBitstreamRedirect;
	pcBitstreamRedirect = new TComOutputBitstream;

	// Append substreams...
	TComOutputBitstream *pcOut = pcBitstreamRedirect;
	std::vector<TComOutputBitstream> substreamsOut(uiNumSubstreams);

	for (UInt ui = 0; ui < uiNumSubstreams; ui++)
	{
		std::vector<uint8_t> &bufIn = ppcSubstreams[ui]->getFifo();
		std::vector<uint8_t> &bufOut = substreamsOut[ui].getFIFO();
		bufOut.resize(bufIn.size());
		bufOut = bufIn;
		pcOut->addSubstream(&(substreamsOut[ui]));
	}

	// Byte-align
	outNalu.m_Bitstream.writeByteAlignment();

	// Perform bitstream concatenation
	if (pcOut->getNumberOfWrittenBits() > 0)
	{
		outNalu.m_Bitstream.addSubstream(pcOut);
	}

	// clean-up
	for (UInt ui = 0; ui < uiNumSubstreams; ui++)
	{
		delete ppcSubstreams[ui];
	}
	delete[] ppcSubstreams;
	delete pcBitstreamRedirect;

	return;

}

Void TAppSubPicExtTop::xWriteOutput(std::ostream& bitstreamFile, const AccessUnit accessUnit)
{
	for (AccessUnit::const_iterator it = accessUnit.begin(); it != accessUnit.end(); it++)
	{
		const NALUnitEBSP& nalu = **it;

		static const UChar start_code_prefix[] = { 0,0,0,1 };
		if (it == accessUnit.begin() || nalu.m_nalUnitType == NAL_UNIT_VPS || nalu.m_nalUnitType == NAL_UNIT_SPS || nalu.m_nalUnitType == NAL_UNIT_PPS)
		{
			/* From AVC, When any of the following conditions are fulfilled, the
			* zero_byte syntax element shall be present:
			*  - the nal_unit_type within the nal_unit() is equal to 7 (sequence
			*    parameter set) or 8 (picture parameter set),
			*  - the byte stream NAL unit syntax structure contains the first NAL
			*    unit of an access unit in decoding order, as specified by subclause
			*    7.4.1.2.3.
			*/
			bitstreamFile.write(reinterpret_cast<const TChar*>(start_code_prefix), 4);
		}
		else
		{
			bitstreamFile.write(reinterpret_cast<const TChar*>(start_code_prefix + 1), 3);
		}
		bitstreamFile << nalu.m_nalUnitData.str();

	}
}
#endif
